package com.example.demo.services.project;

import com.example.demo.model.Project;

public interface ProjectService {

	public Project createProject(Project project);

	public Project getProjectById(long projectId);

	public Project updateProjectById(Project updatedProject, long projectId);

	public Project deleteProjectById(long projectId);

	public Project getProjectList();

}
