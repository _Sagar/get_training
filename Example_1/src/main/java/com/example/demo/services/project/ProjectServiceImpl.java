package com.example.demo.services.project;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.Project;

@Service
public class ProjectServiceImpl implements ProjectService {

	List<Project> projectList = new ArrayList<>();
	
	
	@Override
	public Project createProject(Project project) {
		projectList.add(project);
		return project;
	}

	@Override
	public Project getProjectById(long projectId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Project updateProjectById(Project updatedProject, long projectId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Project deleteProjectById(long projectId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Project getProjectList() {
		// TODO Auto-generated method stub
		return null;
	}

}
