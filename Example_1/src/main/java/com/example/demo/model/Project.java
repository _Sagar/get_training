package com.example.demo.model;

import java.util.Date;

public class Project {
	long id;
	String name;
	String mimeType;
	String version;
	String description;
	Date createdDate;
	Date updatedDate;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Override
	public String toString() {
		return "Project [id=" + id + ", name=" + name + ", mimeType=" + mimeType + ", version=" + version
				+ ", description=" + description + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate
				+ "]";
	}
	
	

}
